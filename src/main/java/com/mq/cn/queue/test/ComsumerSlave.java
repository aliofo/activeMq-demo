package com.mq.cn.queue.test;

import java.io.IOException;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

public class ComsumerSlave {
	
	public static void main(String[] args) {
		// 创建工厂连接对象，需要制定IP和端口号
		ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory("tcp://master:61616");
		try {
			// 使用连接工厂创建链接对象
			Connection connection = activeMQConnectionFactory.createConnection();
			// 开始连接
			connection.start();
			
			// 使用连接对象创建会话对象
			Session session = connection.createSession(Boolean.FALSE, Session.AUTO_ACKNOWLEDGE);
			// 使用会话对象创建目标对象 包含queue和topic（一对一和一对多）
			Queue queue = session.createQueue("test001");
			// 使用会话对象创建消费者对象
			MessageConsumer consumer = session.createConsumer(queue);
			// 向消费者注册监听对象,用来接收消息
			consumer.setMessageListener(new MessageListener() {
				
				@Override
				public void onMessage(Message message) {
					if(message instanceof TextMessage) {
						TextMessage msg = (TextMessage)message;
						try {
							System.out.println(msg.getText());
						} catch (JMSException e) {
							e.printStackTrace();
						}
					}
				}
			});
			
			//程序等待接收消息
			System.in.read();
			
			//关闭资源
			consumer.close();
			session.close();
			connection.close();
		} catch (JMSException | IOException e) {
			e.printStackTrace();
		}
		//
	}

}
