package com.mq.cn.topic.test;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnectionFactory;

public class Producter {
	
	public static void main(String[] args) {
		// 创建工厂连接对象，需要制定IP和端口号
		ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory("tcp://master:61616");
		try {
			// 使用连接工厂创建链接对象
			Connection connection = activeMQConnectionFactory.createConnection();
			// 开始连接
			connection.start();
			// 使用连接对象创建会话对象
			Session session = connection.createSession(Boolean.FALSE, Session.AUTO_ACKNOWLEDGE);
			
			
			// 使用会话对象创建目标对象 包含queue和topic（一对一和一对多）
			Topic topic = session.createTopic("test001-topic-test");
			// 使用会话对象创建生产者对象
			MessageProducer producer = session.createProducer(topic);
			// 使用会话对象创建消息对象
			TextMessage textMessage = session.createTextMessage("test001-topic-test");
			// 发送消息
			producer.send(textMessage);
			
			//关闭资源
			producer.close();
			session.close();
			connection.close();
		} catch (JMSException e) {
			e.printStackTrace();
		}
		//
	}

}
